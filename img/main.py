import pygame
from pygame.locals import *
import waterFX
import time
from waterFX import nxtexture as texture
# Initialize Pygame
pygame.init()
import math
# Define the window dimensions
WIDTH = 800
HEIGHT = 600

# Create the display surface
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Pygame Image")


import math

def sine_mod(value, time_to_repeat, max_amplitude):
    # Calculate the current time as a fraction of the total repeat time
    current_time_fraction = (value % time_to_repeat) / time_to_repeat

    # Calculate the phase shift based on the current time fraction
    phase_shift = current_time_fraction * 2 * math.pi

    # Calculate the value on the sine wave
    sine_value = max_amplitude * math.sin(phase_shift)

    return sine_value

class caustic_texture:
    def __init__(self):
        self.caustic_animationloop = [texture("Caustics/caust_0" + str(x) +".png") for x in range(1,17)]
        self.btexture = waterFX.blur_surface(texture("sludge.png").gt2(),1)
        self.texturecache = [self.gte(x) for x in range(0,32)]
    def gt(self,ft):
        ft = ft%32
        return self.texturecache[ft]
    def gte(self,ft):
        tx=6
        modtime = abs(sine_mod(ft*0.18750117188232426,tx*2,tx))+8
        xed = pygame.transform.smoothscale(pygame.transform.smoothscale(waterFX.apply_waving_effect(waterFX.apply_waving_effect(waterFX.apply_ripple(pygame.transform.smoothscale_by(self.btexture,2),self.caustic_animationloop[(ft%16)].gt2(),2,2),self.caustic_animationloop[((ft+1)%16)].gt2(),5,16,tx-modtime,modtime+1),self.caustic_animationloop[((ft+1)%16)].gt2(),5,16,modtime+1,(1+tx)-modtime,xte=0.5),(251,251)),(560,560))
        pygame.image.save(xed,"img/sludge_"+str(ft)+".png")
        return xed
    
xt = caustic_texture()
image = xt.gt(2)
# Set the initial position of the image
image_x = (WIDTH - image.get_width()) // 2
image_y = (HEIGHT - image.get_height()) // 2

# Set the movement speed of the image
frametime = 0

# Game loop
running = True
while running:
    # Handle events
    image = xt.gt(frametime)
    for event in pygame.event.get():
        if event.type == QUIT:
            running = False
        elif event.type == KEYDOWN:
            if event.key == K_LEFT:
                frametime+=1
                image = xt.gt(frametime)
            elif event.key == K_RIGHT:
                frametime+=-1
                

    # Fill the screen with a white color
    screen.fill((255, 255, 255))
    print(frametime)

    # Blit the image onto the screen
    screen.blit(image, (image_x, image_y))

    # Update the display
    pygame.display.flip()

# Quit Pygame
pygame.quit()