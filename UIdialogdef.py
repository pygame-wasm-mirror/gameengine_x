from dialogtree import UIdialogbase
from dialogtree import pygame
import time
import ptext
from inventory import imxrender ,imvrender
import random
import waterFX


class IPXdraw():
    def __init__(self):
        pass
    def fill(x,y,w,h,bg,screen):
         pygame.draw.rect(screen, bg, (x, y, w, h))
class spaceobserverdia(UIdialogbase):
    def initialise(self):
        self.add_btn("exit","exitbtn",(0.9,0.9),(0.1,0.1))
        self.add_btn("left","left",(0.9,0.8),(0.1,0.1))
        self.add_btn("up","_up_",(0.9,0.7),(0.1,0.1))
        self.add_btn("right","right",(0.9,0.6),(0.1,0.1))
        self.add_btn("down","down",(0.9,0.5),(0.1,0.1))
        self.x = 0
        self.y = 0
        self.initialised = 1
    def renderframe(self):
        global ccredits
        #self.x = self.x +100
        #self.drawsys.screen
        #print(self.x)
        teal = 0.1
        teal2 = 0.9
        self.frametime = (self.frametime + 1)%200
        time.sleep(0.05)
        pygame.draw.rect(self.drawsys.screen, (int(245*teal),int(235*teal),int(250*teal)), pygame.Rect((self.scale(0, 0)+self.scale( 1, 1))))
        pygame.draw.rect(self.drawsys.screen, (int(245*teal2),int(235*teal2),int(250*teal2)), pygame.Rect((self.scale(0.9, 0)+self.scale( 1, 1))))
        t = drawplanetsurface(self.x,self.y,self.scale(0.9,1)[0],self.scale(1,1)[1])
        self.drawsys.screen.blit(t,(0,0))
        
        #ptext.draw( " ", (30,70), shadow=(1.0,1.0), scolor="gold",fontsize=16)
    #def mouseklick(self,x,y):
        #pass
    def btnp(self,name):
        if name == "exitbtn":
            self.active = 0
        if name == "right":
            self.x = self.x +250
        if name == "left":
            self.x = self.x -250
        if name == "_up_":
            self.y = self.y +250
        if name == "down":
            self.y = self.y -250
###################pong
            
            
WIDTH, HEIGHT = 840, 600
BALL_RADIUS = 10
PADDLE_WIDTH, PADDLE_HEIGHT = 10, 60
#screen = pygame.display.set_mode((WIDTH, HEIGHT))
ball_pos = [WIDTH/2, HEIGHT/2]
ball_vel = [6, 4]
paddle_pos = HEIGHT/2 - PADDLE_HEIGHT/2
game_over = False

            
class settingsdia(UIdialogbase):
    def __init__(self,settings):
        UIdialogbase.__init__(self)
        self.settings = settings
        print(settings)
        self.initialised = 0
    def initialise(self):
        self.initialised =1
        #pong = pygame.image.load("img/pong.png")
        #flappy = pygame.image.load("img/flappybox.png")
        self.add_slider("       touch controls ",(300,160),["ON","OFF"],default=(1-self.settings[0]))
        self.add_slider("water Level of detail ",(300,190),["ON","OFF"],default=(1-self.settings[1]))
        self.add_slider("    performance       ",(300,220),["ON","OFF"],default=(1-self.settings[2]))
        self.add_slider("      enable  audio   ",(300,250),["ON","OFF"],default=(1-self.settings[3]))
        self.add_slider("skip credits",(300,280),["ON","OFF"],default=(1-self.settings[4]))
        #self.add_slider("developer mode ",(300,300),["ON","OFF"],default=(1-self.settings[5]))
        #self.add_btn("Pong", "pongBtn", (0.5, 0.3), (0.1, 0.1),text1=pong,text2=pong)
        #self.add_btn("Flappy Bird", "flappyBtn", (0.5, 0.6), (0.1, 0.1),text1=flappy,text2=flappy)

    def renderframe(self):
        self.drawsys.screen.fill((200, 180, 187))
        self.string("settings", 300, 10, width=15, color="white")
    def slider(self,name,value):
        if name == "       touch controls ":
            if value == "ON":
                self.settings[0] = 1
            else:
                self.settings[0] = 0
            #print(value)
            #print(value)
        if name == "water Level of detail ":
            if value == "ON":
                self.settings[1] = 1
            else:
                self.settings[1] = 0
        if name == "    performance       ":
            if value == "ON":
                self.settings[2] = 1
            else:
                self.settings[2] = 0
        if name == "      enable  audio   ":
            if value == "ON":
                self.settings[3] = 1
            else:
                self.settings[3] = 0
        if name == "skip credits":
            if value == "ON":
                self.settings[4] = 1
            else:
                self.settings[4] = 0
        self.val = self.settings
        

       

class invdia(UIdialogbase):#i have not tested this , at least it has a chance of working until tested 
    def __init__(self,x,y):
        UIdialogbase.__init__(self)
        self.cx = x
        self.cy = y
        self.FTIO = 0
    def initialise(self):
        self.active = 1
        self.initialised = 1
        self.inv = {}  # initialize inventory
        self.xi = 0  # initialize xi variable

    def renderframe(self):#####YYYYYYYYYYYYYAAAAAAAAAAAAAAAAYYYYYYYYYYYYYYY it works , i think i know how but just in case leave this alone   
        if self.FTIO == 0:
            self.FTIO == 1
            tl = self.cmap.structuremap.rmmap((self.cx,self.cy),1)
            if not tl == "none":
                if tl.price > 0:
                    self.inv = {}
                    self.inv[tl.mp_item] = 1
                    self.cmap.structuremap.smmap((self.cx,self.cy),"none")
                else:
                    self.cmap.structuremap.smmap((self.cx,self.cy),"none")
                
        self.drawsys.screen.fill((0, 0, 0))
        # render inventory using imvrender function
        xo = imxrender(self.cplayer.inventory.inv, self.inv)
        #print(xo)
        self.cplayer.inv = xo[1]
        self.inv = xo[2]
        time.sleep(0.05)
        if xo[0] ==  1:
            self.active = 0
            print(self.inv)
            if self.inv == {}:
                self.cmap.structuremap.smmap((self.cx,self.cy),"none")
            else:
                nx = next(iter(self.inv.items()))
                tlc = nx[0]
                print(tlc.blockID)
                xi = "none"
                for i in self.cmap.tiles:
                    if i.__class__.__name__ == tlc.blockID:
                        xi = i
                        print("match found ")
                self.cmap.structuremap.smmap((self.cx,self.cy),xi)
                print(str(self.cx) +"+"+str(self.cy))
                
            ##add code that replaces tile just above x/y coords
            #just do it, if it does not work ,you can fix it later


    def btnp(self, name):
        pass  # do nothing on button press

    def runUI(self):
        #while self.active:
            self.renderframe()
            #self.handleinputs()
            pygame.display.flip()

class inv2dia(UIdialogbase):#i have not tested this , at least it has a chance of working until tested 
    def __init__(self,tileinv):
        UIdialogbase.__init__(self)
        #self.cx = x
        #self.cy = y
        self.inv = tileinv
        self.FTIO = 0
    def initialise(self):
        self.active = 1
        self.initialised = 1
        #self.inv = {}  # initialize inventory
        self.xi = 0  # initialize xi variable

    def renderframe(self):#####YYYYYYYYYYYYYAAAAAAAAAAAAAAAAYYYYYYYYYYYYYYY it works , i think i know how but just in case leave this alone   
        #if self.FTIO == 0:
            #self.FTIO == 1
            #tl = self.cmap.structuremap.rmmap((self.cx,self.cy),1)
            #if not tl == "none":
                #if tl.price > 0:
                    #self.inv = {}
                    #self.inv[tl.mp_item] = 1
                    #self.cmap.structuremap.smmap((self.cx,self.cy),"none")
                #selse:
                    #self.cmap.structuremap.smmap((self.cx,self.cy),"none")
                
        self.drawsys.screen.fill((0, 0, 0))
        # render inventory using imvrender function
        xo = imvrender(self.cplayer.inventory.inv, self.inv)
        #print(xo)
        self.cplayer.inv = xo[1]
        self.inv = xo[2]
        time.sleep(0.05)
        if xo[0] ==  1:
            self.active = 0
            self.inv = xo[2]
            #print(self.inv)
            #if self.inv == {}:
                #self.cmap.structuremap.smmap((self.cx,self.cy),"none")
            #else:
               # nx = next(iter(self.inv.items()))
                #tlc = nx[0]
                #print(tlc.blockID)
                #xi = "none"
                #for i in self.cmap.tiles:
                    #if i.__class__.__name__ == tlc.blockID:
                        #xi = i
                        #print("match found ")
                #self.cmap.structuremap.smmap((self.cx,self.cy),xi)
                #print(str(self.cx) +"+"+str(self.cy))
                
            ##add code that replaces tile just above x/y coords
            #just do it, if it does not work ,you can fix it later


    def btnp(self, name):
        pass  # do nothing on button press

    def runUI(self):
        while self.active:
            self.renderframe()
            #self.handleinputs()
            pygame.display.flip()
    
