log_entries = [
    {
        'timestamp': '2070-06-15 09:23:14',
        'message': "Received news today that upper management granted themselves a substantial pay raise. It's disheartening to see such disparity within the company, with executives enjoying lavish rewards while the rest of us struggle without any salary increases for years. It's clear where management's priorities lie, and it's certainly not with the well-being of the employees.",
        'title': 'Pay Raise Disparity'
    },
    {
        'timestamp': '2070-06-16 14:45:32',
        'message': "The CEO just informed us about cost-cutting measures. They want to reduce employee benefits to improve the company's financial outlook. It feels like we're being squeezed to the breaking point, while executives continue to live in luxury.",
        'title': 'Cost-Cutting Measures and Employee Benefits'
    },
    {
        'timestamp': '2070-06-17 10:11:59',
        'message': "Reports of workplace accidents have been pouring in lately. Safety concerns must be addressed promptly; otherwise, someone could sustain serious injuries. However, it's disturbing to discover that management is actively sabotaging regulators' inspections, guiding them only to well-maintained areas while hiding the true risks.",
        'title': 'Sabotaging Regulators\' Inspections'
    },
    {
        'timestamp': '2070-06-18 16:57:42',
        'message': "Whispers about potential unethical practices within our department are growing louder. I fear that safety protocols are being deliberately ignored to cut costs. The health risks associated with our work are mounting, and it seems management is willing to jeopardize our well-being for profit.",
        'title': 'Unethical Practices and Ignored Safety Protocols'
    },
    {
        'timestamp': '2070-06-19 08:30:05',
        'message': "The deadline for our new project seems impossible to meet without compromising quality. It's frustrating to be placed in such a predicament. We need more time or additional resources, but management dismisses our concerns and continues to push for shortcuts that further compromise safety.",
        'title': 'Unrealistic Deadlines and Compromised Safety'
    },
    {
        'timestamp': '2070-06-20 12:09:28',
        'message': "Unfortunately, another round of layoffs was announced today. Morale within the office is at an all-time low, with uncertainty hanging over everyone's heads. It's difficult to stay focused on work when job security feels so tenuous, and the risks we face are intentionally concealed.",
        'title': 'Layoff Announcement and Low Morale'
    },
    {
        'timestamp': '2070-06-21 15:40:50',
        'message': "I stumbled upon some documents indicating a disturbing pattern of management giving orders to get rid of regulators or guide them to well-maintained areas only. It's evident that the company is more interested in maintaining an appearance of compliance rather than addressing the true safety risks we face.",
        'title': 'Manipulating Regulators for Appearance of Compliance'
    },
    {
        'timestamp': '2070-06-22 09:55:17',
        'message': "An email was circulated today regarding possible environmental violations by our organization. It raises concerns about our impact on the ecosystem, as well as potential health risks for employees. But I fear that management will attempt to suppress these concerns, as they have done in the past.",
        'title': 'Environmental Violations and Suppressed Concerns'
    },
    {
        'timestamp': '2070-06-23 14:22:33',
        'message': "Our department's budget has been significantly slashed again. We're expected to do more with fewer resources, including compromised safety measures. It's a daunting challenge to meet the demands while operating on such limited support, and it's clear that management's priorities lie elsewhere.",
        'title': 'Budget Cuts and Compromised Safety Measures'
    },
    {
        'timestamp': '2070-06-24 11:47:59',
        'message': "The HR department sent out an urgent email today, notifying us of a wave of workplace harassment complaints. While this issue deserves attention, it's disheartening to see management swiftly addressing it while neglecting the more significant safety concerns that are compromising our physical well-being.",
        'title': 'Workplace Harassment Complaints and Neglected Safety Concerns'
    },
    {
        'timestamp': '2070-06-25 17:02:41',
        'message': "Rumors are swirling about an upcoming merger with another company. The uncertainty surrounding this potential consolidation is unsettling, leaving everyone on edge about their job security and wondering if the safety issues will be conveniently swept under the rug to appease the new partners.",
        'title': 'Rumors of Merger and Safety Concerns'
    },
    {
        'timestamp': '2070-06-26 10:35:08',
        'message': "Today, I witnessed a coworker being reprimanded unfairly for raising safety concerns. It's disheartening to see favoritism and lack of fairness within the organization, especially when we're risking our health and well-being while management turns a blind eye.",
        'title': 'Unfair Treatment of Coworker Raising Safety Concerns'
    },
    {
        'timestamp': '2070-06-27 13:19:23',
        'message': "The pressure to meet unrealistic sales targets is pushing some employees to engage in unethical practices, including cutting corners on safety protocols. It's disconcerting to witness colleagues compromising their integrity out of fear of retribution or being sacrificed for the company's profits.",
        'title': 'Unrealistic Sales Targets and Compromised Integrity'
    },
    {
        'timestamp': '2070-06-28 16:48:39',
        'message': "The office atmosphere has turned toxic lately. Gossip, backstabbing, and mistrust are rampant, making it even more challenging to address safety concerns. Workers fear retaliation and being labeled as troublemakers if they dare to speak out.",
        'title': 'Toxic Office Atmosphere and Fear of Retaliation'
    },
    {
        'timestamp': '2070-06-29 09:53:55',
        'message': "Upper management just announced a massive bonus for themselves, further widening the wealth gap within the company. The disparity between executives and employees is staggering and demoralizing, especially when we're fighting for safer working conditions and basic fairness.",
        'title': 'Massive Bonuses and Widening Wealth Gap'
    },
    {
        'timestamp': '2070-06-30 14:27:16',
        'message': "Received a memo today discussing the possibility of outsourcing our jobs to cut costs. The future appears uncertain, and the fear of losing employment looms large among the workforce. We worry that safety will become an even lower priority if profit margins are further prioritized.",
        'title': 'Possibility of Job Outsourcing and Safety Concerns'
    },
    {
        'timestamp': '2070-07-01 11:05:44',
        'message': "Our department's research suggests that our products may have severe health risks associated with prolonged exposure. It's a deeply troubling discovery, but we fear management will suppress this information to protect profits at the expense of public health.",
        'title': 'Severe Health Risks Associated with Products'
    },
    {
        'timestamp': '2070-07-02 15:12:19',
        'message': "The work-life balance here is non-existent. Constantly dealing with heavy workloads and long hours is taking a toll on everyone's well-being. Burnout is becoming a prevalent issue, worsened by the lack of attention to safety and the feeling that our lives are considered expendable.",
        'title': 'Poor Work-Life Balance and Burnout'
    },
    {
        'timestamp': '2070-07-03 10:41:37',
        'message': "Stumbled upon some evidence indicating discussions of manipulating regulators or even getting rid of them entirely. It's a shocking revelation that suggests a deeper level of corruption within the company. Our safety concerns and the well-being of the public are being compromised for the sake of profit.",
        'title': 'Manipulating Regulators and Corruption Concerns'
    },
    {
        'timestamp': '2070-07-04 12:50:02',
        'message': "This job used to be my dream, but now I feel trapped within a system that values profit over people. The management's efforts to cover up safety issues, manipulate regulators, and dismiss our fears are disheartening and outright criminal. It's crucial that we find a way to expose this corruption and protect the well-being of employees and the wider community.",
        'title': 'Trapped in a Profit-Driven System and Covering up Safety Issues'
    }
]
suspicious_fake_messages = [
    {
        'subject': 'Important Employee Announcement',
        'message': "Dear Team,\n\nWe are pleased to inform you that, based on our latest safety assessment, the workplace has been deemed fully compliant with industry standards. Your health and well-being remain our top priority, and we will continue to provide a safe environment for everyone.\n\nSincerely,\nUpper Management",
        'title': 'Safety Compliance Announcement'
    },
    {
        'subject': 'Celebrating Our Dedicated Employees',
        'message': "Dear All,\n\nWe want to express our gratitude for your hard work and commitment. Your contributions have been instrumental in making our workplace a beacon of excellence. As a token of our appreciation, we will be distributing small bonuses to all employees as a symbol of our unwavering support for your well-being.\n\nBest regards,\nUpper Management",
        'title': 'Employee Appreciation Bonus'
    },
    {
        'subject': 'Employee Satisfaction Survey Results',
        'message': "Dear Team,\n\nWe are pleased to share the results of the recent Employee Satisfaction Survey. An overwhelming majority of respondents expressed satisfaction with our work environment, employee benefits, and managerial support. Your feedback helps us ensure that we maintain a positive workplace culture that values your growth and success.\n\nWarm regards,\nUpper Management",
        'title': 'Employee Satisfaction Survey Results'
    },
    {
        'subject': 'Enhancing Employee Wellness Initiatives',
        'message': "Dear Team,\n\nWe are excited to announce the launch of our new employee wellness program, designed to promote work-life balance, mental health, and overall well-being. Your health is our priority, and we are committed to providing you with resources and support to thrive both personally and professionally.\n\nKind regards,\nUpper Management",
        'title': 'Employee Wellness Program Launch'
    },
    {
        'subject': 'Invitation to Employee Town Hall Meeting',
        'message': "Dear Team,\n\nYou are cordially invited to an employee town hall meeting where we will address any concerns or questions you may have. We value open communication and want to ensure that everyone's voice is heard. We look forward to engaging in a constructive dialogue with you all.\n\nBest regards,\nUpper Management",
        'title': 'Employee Town Hall Meeting Invitation'
    },
    {
        'subject': 'New Opportunities for Career Development',
        'message': "Dear Team,\n\nWe are excited to introduce new initiatives aimed at enhancing your career growth within the organization. We will be launching training programs, mentorship opportunities, and internal job postings to empower you with the skills and knowledge needed for advancement. Together, we can achieve new heights!\n\nWarm regards,\nUpper Management",
        'title': 'Career Development Initiatives Announcement'
    },
    {
        'subject': 'Companywide Charity Drive',
        'message': "Dear All,\n\nWe are proud to announce our companywide charity drive to support local communities in need. Your generosity and kindness can make a significant impact on the lives of others. Let's come together and contribute to this noble cause.\n\nSincerely,\nUpper Management",
        'title': 'Charity Drive Announcement'
    },
    {
        'subject': 'Recognition for Exceptional Performance',
        'message': "Dear Team,\n\nWe want to acknowledge and celebrate the outstanding performance of our employees. Your dedication and hard work have not gone unnoticed. We will be recognizing exceptional individuals through awards and public appreciation. Thank you for setting new benchmarks!\n\Best regards,\nUpper Management",
        'title': 'Employee Performance Recognition'
    },
]

