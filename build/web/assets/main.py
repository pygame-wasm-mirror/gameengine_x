import asyncio, pygame, PIL, time 
import gc
import  libraries

async def main():
    global libraries
    frame_count=0
    libraries.xxt = 0
    while True:
        frame_count = (frame_count + 1)%5000
        libraries.main()
        pygame.display.flip()
        await asyncio.sleep(0)
        
        #gc.collect(2)

          # Very important, and keep it 0



# This is the program entry point:
asyncio.run(main())
# Do not add anything from here
# asyncio.run is non-blocking on pygame-wasm
