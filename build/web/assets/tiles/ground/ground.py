 
@addtile
class path(tile):
    def upd(self):
        self.lgco(['ground', 0, 0],"path",(0, 255, 255, 255))
        self.wcost = 0
grass1_btxt = ptexture('img/grass/grassbase.png',rescale=0)
grass1_otxt= ptexture('img/grass/grassoverlay.png',rescale=0)
@addtile
class grass1(tile):
    def upd(self): 
        self.lgco(["ground",1,20],'grass1',(255,255,255,255))
        self.height = 1
        self.reflectivity = 2
        self.UAM=1
        self.animated=1
        self.__indcp__=1
    def gtx(self,fn):
        global grass1_btxt,grass1_otxt
        t=waterFX.gslice(grass1_btxt.gt(),(40*((self.pos[0])%10)),40*(self.pos[1]%10),40)
        x=waterFX.gslice(grass1_otxt.gt(),int(40*((self.pos[0]+self._cam_rpos[0])%10)),int(40*((self.pos[1]+self._cam_rpos[1])%10)),40)
        x.set_alpha(100)
        t.blit(x,(0,0))
        return fptexture(t)
