import pygame
import diahandle
import dialogtree
import UIDIA
import asyncio
import random
import os
import math
import copy
import waterFX
import psprite
import traceback
class tile():
    def init(self,n="",color="",args=[]):
        self.lgco(attributes=args,name=n,color=color,a=1)
    def updtmp(self,cmap):
        return cmap
    def upd(self):
        pass
    def initmp(self):
        pass
    def interact(self,cplayer,cmap,message="found \n nothing"):
        return [cplayer,cmap,message]#usefull for modifying the worldmap  , or teleporting the player the last argument is a message 
    
    def gtx(self,fn):
        return self.gt()
        
    def powerevent(self,cmap):
        return cmap
    def gt(self):
        return self.texture
    def callback(self,cmap=0,cplayer=0,test=0):
        if test == 1:
            return 0
        else:
            return [cmap,cplayer]
    def lgco(self,attributes,name,color,a=0): # legacy compatibility
        self.name = name
        self.rstate = [0,0,0,0]
        self.message = self.name
        self.color = color
        if len(attributes) > 3:
            if  "unpassable" in attributes[3]:
                self.walkable = 0
        self.attributes = []
        self.interactable = 0 
        self.texture = ptexture('img/' +self.name+'.png',a)
    def catchtxt(self,name,scale=1):
        return ptexture('img/' +name+'.png',rescale=scale)
    def wirephandle(self):
        pass
    def __init__(self,n="404",color=[0,0,0,0],args=[]):
        self.color = color
        self.walkable = 1
        if len(args) > 3:
            if  "unpassable" in args[3]:
                self.walkable = 0
        self.rstate = [0,0,0,0]
        self.tst = [ptexture('mesecons/S0O.png'),ptexture('mesecons/S1O.png'),ptexture('mesecons/OO.png'),ptexture('mesecons/S3O.png')]
        self.mp_item = None
        self.animated = 0
        self.height = 0
        self.price = 0
        self.PBA = 0
        self.place_last =0
        self.name = n
        self.message = ""
        
        self.hidden = 0
        self.on = 0
        self.conductor = 0
        self.state = 0
        self.x = 0
        self.y = 0
        self.texture = ptexture('img/'+str(self.name)+'.png')
        if not self.name == "404":
            if waterFX.cntp(self.texture.gt())>50:
                self.walkable=0
        #self.texture = ptexture('img/404.png')
        self.textures = []
        self.pos = [0,0]
        self.attributes = ["ground",0,0,[]] ## sample ["detail"[group, if not set is assumed to be ground ],1 [enemy level to spawn 1 is basic enemies , 10 is challenging enemies],10 [chance of spawning (1 in #)],['unpassable','id32'] [list of attributes that can be used elsewhere in code , unpassable means player cannot walk through,"trader" [npc to spawn]]
    def update_state(self,cmap,state,s=""):
        global rstates
        nxt = [(-1,0),(0,1),(1,0),(0,-1)]
        lt = [2,3,0,1]
        self.x = self.pos[0]
        self.y = self.pos[1]
        self.on = state
        self.wirephandle()
        if not s=="":
            dntupd = lt[s]
        else:
            dntupd = ""
        for i in nxt:
            tilex= cmap.read(cmap.structuremap,self.x+i[0],self.y+i[1],True)
            if not tilex == "none":
                if tilex.conductor == 1:
                    if not tilex.on == state:
                        try:
                            #if not x == dntupd:
                                cmap = tilex.update_state(cmap,state)#,s=x)
                                cmap.structuremap = cmap.sett(cmap.structuremap,self.x+i[0],self.y+i[1],tilex)
                        except Exception as e:
                            print("NONCONDUCTING TILE HAS update_state function ")
                            print(e)
            
                else:
                    try:
                        tilex.rstate[lt[nxt.index(i)]] = state
                        cmap = tilex.powerevent(cmap)
                        cmap.structuremap = cmap.sett(cmap.structuremap,self.x+i[0],self.y+i[1],tilex)
                    except Exception as e:
                        print(e)
                        
        
        return cmap



class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
class common(metaclass=Singleton):
    xtiles=[]
    entities=[]
    tile=tile
    companion = "none"
    disptm = 0
    printatall = 1
    pygame=pygame
    traceback=traceback
    psprite=psprite
    waterFX=waterFX
    copy=copy
    os=os
    math=math
    random=random
    asyncio=asyncio
    UIDIA=UIDIA
    dialogtree=dialogtree
    pygame = pygame
    def __init__(self):
        self.pygame=pygame
        self.traceback=traceback
        self.psprite=psprite
        self.waterFX=waterFX
        self.copy=copy
        self.math=math
        self.random=random
        self.asyncio=asyncio
        self.UIDIA=UIDIA
        self.dialogtree=dialogtree
        self.pygame = pygame
        self.os=os
    def addentity(self,tilexx):
        try:
           entity = tilexx()
        except Exception as ie:
            entity = tilexx()
            print()
        self.entities.append(entity)
        return tilexx
    def addtile(self,tile):
        tile = tile()
        tile.upd()
        self.xtiles.append(tile)
        return tile
    def get_tiles(self):
        tiles = []
        for itile in self.xtiles:
            itile.upd()
            if itile.price > 0:
                #testlist.append((itile))
                it = self.dialogtree.inv_handle.item("tile_" + str(itile.name),"this item allows you to costumise any \n applicable tiles with \n a"+str(itile.name),ptextpath=itile.gt().location,blockid=itile.__class__.__name__)
                self.dialogtree.inv_handle.possible_items["tile_"+str(itile.name)] = it
                #print("tile_"+str(itile.name))
                itile.mp_item = it
            tiles.append(itile)
        return tiles
            
            
def IO():
    pass 
class sttobj():
    def __init__(self,x,y):
        self.x = x
        self.y = y
        self.state = {"x":x , "y": y}
    def sets(key,value):# index and value to place at index
        self.state[key] = value
    def gets(key,default): # index  , and value to return if index does not have a value
        if key in self.state:
            return self.state[key]
        else:
            return default
quests = {}
tile_textures = {}#optimisation to not need a rtx 4090 XYT


    
class fptexture(): # a texture pointer class
    def __init__(self,image):
        global tile_textures
        imgstr = hash(pygame.image.tostring(image, 'RGBA'))
        if not str(imgstr) in tile_textures:
            #print("intitialising_ptexture")
                tile_textures[str(imgstr)] = image
                tile_textures[str(imgstr)+"0.5x"] = pygame.transform.scale_by(image,0.5)

        self.location = str(imgstr)
    def gt(self):
        return tile_textures[self.location]

    

class entity():
    def __init__(self,x,y):
        self.pos = [x,y]
        self.delme = 0
        self.message=""
        self.dt = 20
        self.frametime = 0
        self.swm =0 #save  with map
        self.texture = ptexture('img/footstep.png')
    def run_s(self,tiles,cmap,code):
        if  code=="MO":
            if self.ht==0:
                self.itp=pygame.time.get_ticks()
            self.ht=2
        elif code=="INT":
            self.intcallback(tiles,cmap)
        return self
    def intcallback(self,tiles,cmap):
        pass
    def drawdialog(self,x,y):
        dia_alpha=0
        if self.itp>0:
            dia_alpha=min(255,pygame.time.get_ticks()-self.itp)
        if self.itp<0:
            dia_alpha=max(0,255-pygame.time.get_ticks()-abs(self.itp))
        if dia_alpha>0:
            self.diadisplay.text=self.message
            surf=self.diadisplay.draw()
            dtx=surf.get_width()
            screen.blit(surf,(x,y))
    def draw(self,x,y,screen,dscale=0):
        global quests
        if "bgtrailcolor" in quests:
            color = list(color_name_to_rgb(quests["bgtrailcolor"],self.frametime))
        else:
            quests["bgtrailcolor"] = "black"
            color = list(color_name_to_rgb(quests["bgtrailcolor"],self.frametime))
        color[3] =  100
        self.frametime += 10
        self.frametime = self.frametime % 100
        try:
            #screen.blit(pygame.transform.scale(self.texture.gt(),(self.dt,self.dt)),(int(x-(self.dt*0.5)),int(y-int(self.dt*0.5))))
            if dscale==0:
                screen = draw_circle(screen, color, (x, y), int(self.dt*0.5))
            else:
                screen = draw_circle(screen, color, (x, y), int(self.dt*0.25))
        except:
            self.delme = 1
        return screen
    def run(self,tiles,cmap):
        time = pygame.time.get_ticks()
        if self.ht<1 and self.itp>0:
            self.itp=0-time
        if self.ht>0:
            self.ht+=-1
        self.dt = self.dt - 1
        if self.dt < 2:
            self.delme = 1
        return self
    def rm(self):
        self.delme = 1
    
CST=common()
CST.entity =entity
CST.tile_textures = {}
class ptexture(): # a texture pointer class
    def __init__(self,location,a=1,rescale=1,xd=1):
        global CST
        if not str(location) in CST.tile_textures:
            #print("intitialising_ptexture")
            if "brick" in location:
                xd=0
            try:
               preimg = pygame.image.load(str(location)).convert_alpha()
            except Exception as ue:
                print(ue)
                preimg = pygame.surface.Surface((40,40))
                preimg.fill((random.randint(0,255),random.randint(0,255),random.randint(0,255),255))
            if rescale and 1==xd:
                CST.tile_textures[str(location)] = pygame.transform.scale(preimg, (40,40))
                
            else:
                CST.tile_textures[str(location)] = preimg
            CST.tile_textures[str(location)+"0.5x"] = pygame.transform.scale_by(preimg,0.5)
            del(preimg)
        self.location = str(location)
    def gt(self):
        return CST.tile_textures[self.location]
CST.ptexture = ptexture
CST.fptexture = fptexture
CST.addentity = CST.addentity
CST.addtile = CST.addtile
CST.tile=CST.tile
CST.entitydia=diahandle.entitydia
CST.exampledia = diahandle.npcdia1
from nav_ent import nav_ent
CST.nav_ent = nav_ent