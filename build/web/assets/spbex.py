import pygame ,ptext
pygame.init()
surface = pygame.display.set_mode((480,480))
fcolor=(98,0,238)
bcolor = (61, 61, 61,100)
def gettt(text, width, time_characters):
    if len(text)>width:
        num_characters = len(text)
        start_index = time_characters % num_characters
        end_index = (start_index + width) % num_characters

        if start_index <= end_index:
            result = text[start_index:end_index]
        else:
            result = text[start_index:] + text[:end_index]

        return result
    else:
        return text
def fs(string, n):
    return string[:n] + ' ' + string[n:]
class speechbubble:
    def __init__(self,text):
        self.text=text
        self.presurf = pygame.surface.Surface((128,40)).convert_alpha()
        self.presurf.fill((0,0,0,0))
        pygame.draw.rect(self.presurf,bcolor,(0,0,128,40),0,13)
    def draw(self):
        print(int(pygame.time.get_ticks()*0.01))
        text = fs(gettt(self.text,30,int(pygame.time.get_ticks()*0.01)%len(self.text)),15)
        surf = self.presurf.copy()
        ptext.draw(text,(2,3),fontsize=16,scolor=fcolor,antialias=True,shade=True,shadow=(1,1),width=128 ,surf=surf)
        return surf




bubble = speechbubble("Hello Olivia this is just text")

# Set up the display
screen = pygame.display.set_mode((800, 600))
clock = pygame.time.Clock()

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    
    # Clear the screen
    screen.fill((0, 0, 255))
    
    # Draw the speech bubble
    bubble_surf = bubble.draw()
    screen.blit(bubble_surf, (0, 0))
    
    # Update the display
    pygame.display.flip()
    clock.tick(60)

pygame.quit()