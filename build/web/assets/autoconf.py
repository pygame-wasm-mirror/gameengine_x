import pygame,waterFX,math,os
color = int(input("enter IID"))
name = input("enter tile name ")
image = pygame.image.load("image.png")
surface = pygame.surface.Surface((math.ceil(image.get_width()/40)*40,math.ceil(image.get_width()/40)*40),pygame.SRCALPHA)
xc = [int(x/40) for x in  surface.get_size()]
print(xc)
surface.blit(image,(0,0))
os.mkdir(name)
surface2 = pygame.surface.Surface(xc)
strt = ""
for x in range(0,xc[0]):
    for y in range(0,xc[0]):
        surface2.set_at((x,y),(color,x,y,255))
        img=waterFX.gslice(surface,x*40,y*40,40,40)
        tsurface = pygame.surface.Surface((40,40),pygame.SRCALPHA)
        img.set_colorkey((0,0,0,255))
        tsurface.blit(img,(0,0))
        pygame.image.save(tsurface,name+'/'+name+str(x)+str(y)+'.png')
        strt += ",tile('CP/"+name+'/'+name+str(x)+str(y)+"',color=(" +str(color)+","+str(x)+","+str(y)+",255))"
pygame.image.save(surface2,name+"/schematic.png")
with open(name+"/out.txt",'w') as f:
    f.write(strt)
