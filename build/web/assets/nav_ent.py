from common import CST
globals().update(vars(CST))
def extract_sprite_from_32x_cat_spritesheet(spritesheet, orientation, frame):
    sprite_width = 40
    sprite_height = 40
    num_columns = 3
    
    row_offset = 0
    if orientation == 'left':
        row_offset = 3
    elif orientation == 'right':
        row_offset = 1
    elif orientation == 'up':
        row_offset = 2
    elif orientation == 'down':
        row_offset = 0
    elif orientation == 'standright':
        row_offset = 0
        frame=0
    elif orientation == 'standleft':
        row_offset = 0
        frame=0
        
    # Calculate the position in the spritesheet
    column = (frame ) % num_columns
    row = row_offset
    
    # Calculate the position in pixels
    x = column * sprite_width
    y = row * sprite_height
    
    # Extract the sprite from the spritesheet
    #print(pygame.Rect(x, y, sprite_width, sprite_height))
    #print((spritesheet.get_width(),spritesheet.get_height()))
    sprite = spritesheet.subsurface(pygame.Rect(x, y, sprite_width, sprite_height))
    
    return sprite
def determine_direction(start, end):
    start_x, start_y = start
    end_x, end_y = end
    
    if start_x < end_x:
        return 'right'
    elif start_x > end_x:
        return 'left'
    elif start_y < end_y:
        return 'down'
    elif start_y > end_y:
        return 'up'
    else:
        return 'right'  # Default direction if the coordinates are the same
def determine_dir2(start, end):
    start_x, start_y = start
    end_x, end_y = end
    
    if start_x < end_x:
        return 'standleft'
    elif start_x > end_x:
        return 'standright'
    elif start_y < end_y:
        return 'standright'
    elif start_y > end_y:
        return 'standleft'
    else:
        return 'standright'  # Default direction if the coordinates are the same
class AIM(type):
    def __call__(cls, *args, **kwargs):
        instance = super().__call__(*args, **kwargs)
        for superclass in cls.__bases__:
            if hasattr(superclass, '__init__'):
                superclass.__init__(instance)
        return instance
class nav_ent(metaclass=AIM):
    def __init__(self):
        self.pos = [0,0]
        self.delme = 0
        self.dt = 10
        self.itp=0
        self.ht=0
        self.diadisplay=waterFX.speechbubble("")
        self.animation = 0
        self.stage = 1
        self.stnd=0
        if not hasattr(self,'dialog'):
              self.dialog = None
        self.progress = 10
        self.previouspos = self.pos
        self.nextpos = self.pos
        self.swm =0 #save  with map
        self.counter=10
        self.lockedpos=[]
        self.starttime=0
        if not hasattr(self,'message'):
           self.message=""
        self.direction="down"
        
        
        if not hasattr(self,'speed'):
            self.speed=1
        xgender="Girl"
        if hasattr(self,'gender'):
            xgender=self.gender
        xseed = random.randint(1,1000.0)*32
        if hasattr(self,'seed'):
            xseed = self.seed
        if hasattr(self,'spritesheetx'):
            self.texture = self.spritesheetx
        else:
            self.texture = fptexture(psprite.Sprite(xseed,xgender).Spritesheet)
        self.path = []
    def addspritesheet(self,texture):#ptexture compliant texture wrapper
        self.spritesheetx = texture
    def run_s(self,tiles,cmap,code):
        if  code=="MO":
            if self.ht==0:
                self.itp=pygame.time.get_ticks()
            self.ht=2
        elif code=="INT":
            self.intcallback(tiles,cmap)
        else:
            if self.dialog!=None:
                if self.dialog.active==1:
                     self.dialog.interact(str(code))
        return self
    def intcallback(self,tiles,cmap):
        pass
            
    def setchar(self,name,gender,seed):
        self.name = name
        self.gender=gender
        self.seed=seed
    def draw(self,x,y,screen,dscale=0):
        texture =  self.texture.gt()
        dia_alpha=0
        if self.itp>0:
            dia_alpha=min(255,pygame.time.get_ticks()-self.itp)
        if self.itp<0:
            dia_alpha=max(0,255-pygame.time.get_ticks()-abs(self.itp))
        if dia_alpha>0:
            self.diadisplay.text=self.message
            surf=None
            if not self.dialog ==None:
                surf=self.diadisplay.draw2(self.dialog.question,self.dialog.message)
            else:
                surf=self.diadisplay.draw()
            
            dtx=surf.get_width()
            screen.blit(surf,(int((x+20)-(dtx*0.5)),int((y-20)-int(self.dt*0.5))))

        if dscale == 0:
            xtd=1
        else:
            xtd=0.5
        if self.previouspos.__class__.__name__ == 'int' or self.nextpos.__class__.__name__ == 'int':
            return screen
        self.dt = 40
        if len(self.path) < 1 or self.stnd:
            #self.animation = (self.animation + 0.2) % 4
            texture = extract_sprite_from_32x_cat_spritesheet(texture,self.direction,0)
        else:
            if not (self.pos[0]==self.nextpos[0] and self.pos[1]==self.nextpos[1]):
               self.direction = determine_direction(self.pos,self.nextpos)
            texture = extract_sprite_from_32x_cat_spritesheet(texture,self.direction,int(self.progress )% 4)
        try:
            screen.blit(pygame.transform.scale(texture,(int(self.dt*xtd),int(self.dt*xtd))),(int((x+20)-(self.dt*0.5)),int((y+20)-int(self.dt*0.5))))
        except Exception as iex:
            self.delme = 1
            print(iex)
            
        return screen
    def int(self,x,y,i):
        return ((y-x)*i)+x
    def run(self,tiles,cmap):
        time = pygame.time.get_ticks()
        if self.ht<1 and self.itp>0:
            self.itp=0-time
        if self.ht>0:
            self.ht+=-1
        try:
            if self.stage > (len(self.path)) and ((not cmap.islocked(self.nextpos) )or self.nextpos in self.lockedpos):
                t=self.fpath(cmap)
                if str(type(t))== '''<class 'list'>''' or str(type(t))== '''<class 'tuple'>''':
                    self.targetpos= t
                else:
                    return self

    
                prepath = cmap.path(self.pos,self.targetpos)
                
                if not (prepath == None or prepath == []):
                    self.path = prepath
                    self.previouspos = [math.floor(xu) for xu in self.pos]
                    #if len(prepatb)
                    self.nextpos = self.path[0]
                    self.stage = 0
                    
                    #print(prepath)
               
            else:
                    
                if not self.path == []:
                    
                    if not((not cmap.islocked(self.nextpos) )or self.nextpos in self.lockedpos):
                        self.startime=time
                        self.nextpos = self.previouspos
                    self.progress = abs((time-self.starttime)*self.speed)
                    if self.progress < 1000*math.hypot(self.previouspos[0]-self.nextpos[0], self.previouspos[1]-self.nextpos[1]) :
                        self.pos = list(self.pos)
                        self.stnd=0

                        self.pos[0] = self.int(self.previouspos[0],self.nextpos[0],self.progress*0.001)
                        self.pos[1] = self.int(self.previouspos[1],self.nextpos[1],self.progress*0.001)
                    else:
                        self.pos=[round(i) for i in self.pos]

                        if (not cmap.islocked(self.nextpos) )or self.nextpos in self.lockedpos :
                            self.starttime=time
                            self.previouspos = self.pos
                            self.stage = self.stage + 1
                            if (self.stage>len(self.path)):
                               self.nextpos = self.targetpos
                               
                               
                               return self
                            elif len(self.path)==1:
                                self.nextpos = self.path[0]
                                self.previouspos=self.pos
                                self.stage+=90
                                
                                

                                

                            
                            
                            
                            
                            if len(self.lockedpos)<2:
                                cmap.lock(self.nextpos)
                                self.lockedpos.append(copy.deepcopy(tuple(self.nextpos)))
                            else:
                                cmap.unlock(self.lockedpos.pop(0))
                                cmap.lock(self.nextpos)
                                self.lockedpos.append(copy.deepcopy(tuple(self.nextpos)))
                        else:
                            self.stnd=1
                            #print("locked")
                            #print(self.lockedpos)
                            #print("my pos")
                            #print(self.nextpos)
                            for i in self.lockedpos:
                                cmap.unlock(i)
                        
                else:
                    io=0
                    
        except Exception as ie:
            #self.pos = self.targetpos
            self.progress = 9999999
            print(ie)
            print(traceback.format_exc())
                
            
        #self.dt = self.dt - 1
         
        #if self.dt < 2:
            #self.delme = 1
        return self
    def rm(self):
        self.delme = 1