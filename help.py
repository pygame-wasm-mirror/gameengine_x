import pygame

def split_image(source_image_path):
    source_image = pygame.image.load(source_image_path)
    image_width, image_height = source_image.get_size()
    chunks = []
    
    chunk_size = 40
    num_chunks_x = image_width // chunk_size
    num_chunks_y = image_height // chunk_size
    
    for y in range(num_chunks_y):
        for x in range(num_chunks_x):
            chunk_rect = pygame.Rect(x * chunk_size, y * chunk_size, chunk_size, chunk_size)
            chunk_image = pygame.Surface(chunk_rect.size, pygame.SRCALPHA)
            chunk_image.blit(source_image, (0, 0), chunk_rect)
            chunks.append(chunk_image)
    
    return chunks

def generate_addtile_statements(source_image_path,chunks, base_color):
    addtile_statements = []
    source_image_path=source_image_path.replace('.png','')
    for i, chunk in enumerate(chunks):
        x = i % 40
        y = i // 40
        color = (base_color,  x,  y, 255)
        statement = "addtile(tile('" + source_image_path +"{}{}{}', {}, ['ground', 1, 20]))".format(base_color, x, y, color)
        addtile_statements.append(statement)
    
    return addtile_statements

def generate_image(chunks,img_sizex,img_size_y):
    num_chunks_x = 4
    num_chunks_y = 4
    chunk_size = 40
    image_width = num_chunks_x * chunk_size
    image_height = num_chunks_y * chunk_size
    image = pygame.Surface((image_width, image_height), pygame.SRCALPHA)
    
    for i, chunk in enumerate(chunks):
        x = i % num_chunks_x
        y = i // num_chunks_x
        image.blit(chunk, (x * chunk_size, y * chunk_size))
    
    return image

def main():
    source_image_path = input("path_to_source")
    base_color = input("base_color")  # Adjust the base color value from 0 to 255
    
    pygame.init()
    pygame.display.set_mode((1, 1))  # Dummy display mode for image generation
    
    chunks = split_image(source_image_path)
    addtile_statements = generate_addtile_statements(source_image_path,chunks, base_color)
    generated_image = generate_image(chunks,)
    
    for statement in addtile_statements:
        print(statement)
    
    pygame.image.save(generated_image, "gimg.png")
    
    pygame.quit()

if __name__ == "__main__":
    main()