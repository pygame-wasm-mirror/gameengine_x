
import pygame
import pygame_light2d as pl2d
import numpy as np
import cv2
import time
from types import MethodType
from pygame_light2d import LightingEngine, PointLight, Hull
import pygame
import os
# os.environ['SDL_FRAMEBUFFER_ACCELERATION']='1'
# os.environ['PYGAME_BLEND_ALPHA_SDL2']='1'
# #os.environ['SDL_VIDEODRIVER']='opengl'
# os.environ['SDL_HINT_FRAMEBUFFER_ACCELERATION']='1'
# os.environ['SDL_HINT_QTWAYLAND_WINDOW_FLAGS']='1'

X = 840
Y = 640

def scalepoint(point,scale):
    return [(x * scale) for x in point]
def getlines(pygame_image,scale=40):
    pygame_array = pygame.surfarray.array3d(pygame_image)
    opencv_array = np.transpose(pygame_array, (1, 0, 2))
    image = cv2.cvtColor(opencv_array, cv2.COLOR_RGB2BGR)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    _garbage, threshold = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)
    contours, hierarchy = cv2.findContours(threshold, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    simplified_contours = []
    for cnt in contours:
        epsilon = 0.005 * cv2.arcLength(cnt, True)
        approx = cv2.approxPolyDP(cnt, epsilon, True)
        simplified_contours.append(approx)
    all_lines=[]
    for cnt in simplified_contours:
        s=tuple((i*scale) for i in list(cnt[0][0]) )
        lines=[]
        for i in range(1,len(cnt)):
            lines.append([s, scalepoint( list(cnt[i][0]),scale) ])
            s=scalepoint(tuple(cnt[i][0]),scale)
        lines.append([s,scalepoint(list(cnt[0][0]),scale)])
        all_lines+=[lines]
    return all_lines 
def replace_colors(surface, threshold):
    """
    Replaces all colors with RGB values below the given threshold with (0, 0, 0)
    and all other colors with (255, 255, 255).
    
    Args:
        surface (pygame.Surface): The surface to modify.
        threshold (int): The RGB value threshold.
    
    Returns:
        pygame.Surface: The modified surface.
    """
    width, height = surface.get_size()
    pixels = pygame.PixelArray(surface)
    
    for x in range(width):
        for y in range(height):
            pixel = (255, 255, 255)
            r, g, b, a = surface.get_at((x,y))
            if r < threshold:
                pixel = (0, 0, 0)
            surface.set_at((x,y),pixel)
                
    
    return pixels.make_surface()

def get_lmps(texture):
    colors=gtuc(texture)
    heights=[]
    maps={}
    for i in colors:
        heights.append(list(i)[0])
    for i in heights:
        cmap=texture.copy()
        maps[i]=getlines(replace_colors(cmap,i+1))
    return maps
        
def gtuc(surface):
    unique_colors = set()
    for x in range(surface.get_width()):
        for y in range(surface.get_height()):
            color = surface.get_at((x, y))
            unique_colors.add(tuple(color))
    return unique_colors
class light:
    def __init__(self,pos,size=40,color=(255,255,255),tile_pos=None,tile_size=40):
        self.pos=pos
        if (tile_pos):
            self.pos= [tile_pos[0]+(self.pos[0]*tile_size),tile_pos[1]+(self.pos[1]*tile_size)]
        self.pos=[int(i) for i in self.pos]
        #print(self.pos)
        self.size=size*tile_size
        self.color= color
        self.hmpos=[int(i/tile_size) for i in self.pos]
def render_to_buffer(self):##hack to get pygame light2d to render to texture (hopefully)
        self.ctx.screen.clear(0, 0, 0, 1)
        self._fbo_ao.clear(0, 0, 0, 0)
        self._buf_lt.clear(0, 0, 0, 0)

        # Send hull data to SSBOs
        self._send_hull_data()

        # Render lights onto double buffer
        self._render_to_buf_lt()

        # Blur lightmap for soft shadows and render onto aomap
        self._render_aomap()    
frfs=0   
lights_engine=None       
def RTX(lights,HM):
    global lights_engine,frfs
   # print(1)
    heightmap=HM.copy()
    '''
    returns a image with all lights rendered
    input list of lights of light class
    '''
    native_res = list((40*i for i in heightmap.get_size()))
    native_res=(X,Y)
    if lights_engine==None or (frfs==1):
        #def set_screen(self, value):
          # s#elf.screen = value
        frfs=0
        
        
        lights_engine = LightingEngine(screen_res=(X,Y), native_res=(X,Y), lightmap_res=native_res)
        #lights_engine.ctx.set_screen = MethodType(set_screen, lights_engine.ctx)
        light_text=pygame.surface.Surface(native_res)
        #lights_engine.ctx.screen=light_text
    lights_engine.clear(0, 0, 0,0)
    MAPS=get_lmps(heightmap)
    lights_engine.lights=[]
    fakelight = PointLight(position=(500,500), power=1., radius=0)
    fakelight.set_color(0, 0, 0, 0)
    
    for i in lights:
        try:
            height=list(HM.get_at(i.hmpos))[0]
        except:
            height=list(HM.get_at((0,0)))[0]
        lights_engine.hulls=[]
        for ixt in MAPS[height]:
           for ix in ixt:
                lights_engine.hulls.append(Hull(ix))
            
        i.pos[1]=native_res[1]-i.pos[1]
        light = PointLight(position=i.pos, power=1., radius=int(i.size))
        light.set_color(i.color[0], i.color[1], i.color[2], 200)
        #print(i.pos)
        lights_engine.lights.append(light)
    lights_engine.lights.append(fakelight)
    #lights_engine.render()
    render_to_buffer(lights_engine)
    texture_width, texture_height = lights_engine._buf_lt.tex.size
    texture_data = "placeholder if this ever causes a crash then idfk"
    if( lights_engine._buf_lt._ind==1):
        texture_data = lights_engine._buf_lt._fbo1.read(components=4)
    else:
        texture_data = lights_engine._buf_lt._fbo2.read(components=4)
    
    surface = pygame.image.frombytes(texture_data, (texture_width, texture_height), 'RGBA')
    return surface
    
    
    
def overlay_complete_screen(screen):##this is a hack but it maybe works ????       
        img=lights_engine.surface_to_texture(screen)
        lights_engine.render_texture(img, pl2d.FOREGROUND, pygame.Rect(0,0,img.size[0],img.size[1]), pygame.Rect(0,0,img.size[0],img.size[1]))
        lights_engine._render_foreground()   
if __name__ == '__main__':
    pygame.init()
    # Example usage
    HM=pygame.image.load("heightmap_texture.png")
    clock = pygame.time.Clock()
    screen = pygame.display.set_mode((640, 480))
    running = True
    

    while running:
        # Tick the clock at 60 frames per second
        clock.tick(60)
        
        RTX([light(pygame.mouse.get_pos())],HM)
        
        

        

        screen.blit(surface,(0,0))

        pygame.display.flip()
        time.sleep(0.05)

        # Game logic here!
        # ...

        # Process events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
