import ctypes
class entity():
    def init(self,pos,oid=None):
        self.od = oid
class EntityMap():
    def __init__(self):
        self.mmap = {}
        self.ref = {}
    def read(self,pos):
        key=tuple([int(x/CELL_SIZE) for x in pos])
        if key in self.mmap:
            return self.mmap[key]
        else:
            return []
    def gid(self,ID):
        if ID in self.ref:
            return self.mmap[self.ref[ID]]
        else:
            raise IndexError("Entity ID could not be found \n\t    ⢸⠋⠉⣿⠉⠉⣿⠀⠐⡏⠉⠉⢻⡏⠉⠉⢿⢰⡏⠉⠉⢹⡀⡏⠉⢉⠉⠙⢷⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣸⠀⠀⢺⠀⠀⣚⣀⣠⡇⠀⠀⠘⠁⠀⠀⣻⣼⠀⢰⡀⠈⣇⡇⠀⢸⡇⠀⢸⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n⠀⠀⠀⠀⠀⠀⠀⠀⣠⡾⢿⠀⠀⣻⠀⠀⣿⢉⣽⡇⠀⢧⠀⠀⡇⠀⢼⡿⠀⢸⡇⠀⢿⡇⠀⢸⡇⠀⢸⣇⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n⠀⠀⠀⠀⠀⠀⢠⣾⠏⠀⢸⡄⠀⢿⠀⠀⣿⣡⠴⡇⠀⣹⠀⢠⡇⠀⢺⡇⠀⢀⡅⠀⢸⡇⠀⠸⠇⠀⢸⡏⠙⠷⣦⣄⡀⠀⠀⠀⠀⠀\n⠀⠀⠀⠀⠀⢠⣿⠃⠀⠀⢈⡷⢤⣀⣠⣼⠧⠖⠚⢧⡤⠼⢧⠼⢧⡤⠽⢤⡤⠼⠧⣤⣬⢧⡤⢤⠤⠤⢾⡁⠀⠀⠈⠙⢿⡆⠀⠀⠀⠀\n⠀⠀⠀⠀⢠⣿⠃⠀⠀⠀⠀⢠⠊⢠⠊⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡇⠀⠀⠀⠀⠀⢠⡞⠀⠀⠀⠀⠀⠀⠙⡄⠀⠀⠀⢸⣧⠀⠀⠀⠀\n⠀⠀⠀⣠⣾⠏⠀⠀⠀⠀⠀⠀⠀⠀⣠⣴⣾⣿⣿⣿⡿⠿⢶⣦⣄⠀⠀⠀⠀⠀⠀⠨⠀⢀⣀⣠⣤⣤⣀⠀⠀⠀⠀⠀⠈⢿⣆⡀⠀⠀\n⠀⣠⡾⣿⠿⠎⠍⠢⡀⠀⠰⠶⠃⢸⣿⡶⠿⠿⠿⠿⢷⣦⣄⡈⣻⣷⠀⠀⠀⢠⣤⣠⣾⣿⣿⣿⡿⠿⠿⠷⠠⠖⠶⠤⠤⣌⠻⣷⣄⠀\n⣾⢋⡞⠁⣠⡶⠟⠛⢛⠿⣷⣦⣄⣀⡀⣀⣠⣴⡟⠁⠀⠀⠉⠛⠛⠁⠀⠀⠀⠀⠉⢻⡏⠁⠀⠀⠀⠀⠀⠀⢀⣠⣤⣄⡙⢢⡙⣌⢿⡇\n⡏⢸⠀⣰⡟⠀⠀⢀⣿⣄⣀⠈⠉⠉⠛⠉⠉⠁⠀⠀⠀⠀⠀⠀⣀⠀⠀⠀⠀⠀⠀⢹⣷⣄⠀⠀⠀⠿⣶⣾⡟⢩⡍⠉⠛⠀⡇⠸⣿⡇\n⡇⢸⡀⢹⣇⢠⠾⢿⣏⠉⠛⠿⣶⣤⣀⡀⠀⠀⠤⠤⠄⠚⣾⠟⠛⣃⣀⡀⠀⠀⠀⠀⠈⢻⣷⣤⡀⠀⠀⠀⠀⢸⣧⠀⠀⡴⠃⣸⣿⠃\n⣷⡄⢧⡀⠻⠀⠀⠈⣿⣦⣀⠀⠀⢹⡿⠻⠿⣶⣤⣄⣀⠀⠻⣷⠘⠛⠛⠛⠃⢀⣀⢀⣴⠿⠉⠃⠈⠑⠂⢀⣤⣿⣿⣧⠀⠞⢋⣿⠃⠀\n⡉⠻⣦⣍⠀⠀⠀⠀⠈⢿⣿⠻⢶⣾⣿⣤⡀⠀⠀⠉⠙⣿⠿⠶⣶⣦⣤⣤⣄⣀⣙⣛⣁⣀⣀⣤⣤⣶⡾⠿⣿⠹⣿⣿⠀⢀⣿⠏⠀⠀\n⠇⠀⠉⢿⣦⠀⠀⠀⠀⠀⠻⣧⡀⢹⣿⠿⢿⣿⣶⣦⣼⣿⠀⠀⠀⠀⠈⢻⡏⠉⠙⠛⣿⡏⠉⠉⠉⣿⡄⢀⣿⣤⣿⣿⠀⢨⣿⠀⠀⠀\n⠀⠀⠀⠀⢻⣇⠀⠀⠀⠀⠀⠘⢿⣾⡏⠀⠀⠈⠉⠛⣿⠿⣿⣿⣷⣶⣶⣿⣷⣶⣶⣶⣿⣶⣶⣶⣿⣿⣿⣿⣿⣿⣿⣿⠀⢸⣿⠀⠀⠀\n⠀⠀⠀⠀⠈⢿⣦⠀⠀⠀⠀⠀⠀⠙⠿⣷⣄⡀⠀⣰⡿⠀⠀⠈⠉⠙⢻⡿⠿⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢿⣿⣿⡇⠀⢈⣿⠀⠀⠀\n⠀⠀⠀⠀⠀⠀⠙⢿⣦⡀⠤⣀⡀⠠⣀⡈⠙⠿⢶⣿⣅⣀⠀⠀⠀⠀⣾⡇⠀⠀⠀⣸⡏⠀⢀⣾⠇⢠⣿⢁⣿⣿⠟⠀⠀⠀⣿⠀⠀⠀\n⠀⠀⠀⠀⠀⠀⠀⠀⠛⢿⣤⣘⠻⣧⣀⣛⣧⣄⡀⣛⣻⣿⣿⣿⣧⣤⣿⣤⣄⣀⣠⣿⣄⣠⣼⣿⣤⣿⣿⡿⠿⠃⠀⠀⠀⠀⣿⡇⠀⠀\n⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠛⢿⠁⠈⣀⠉⠙⣿⡇⠀⢁⡈⠉⣧⣽⠋⢀⡀⠉⢻⣹⠋⠁⣤⠈⠹⡄⠀⠀⣠⠆⠀⢠⡄⠀⢻⡇⠀⠀\n⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⠀⠀⠿⠀⢀⣿⡇⠀⠸⠇⠀⣼⡇⠀⢸⡇⠀⢸⡿⣤⣤⣿⠀⠀⡷⠚⠋⠁⣀⣠⠜⠀⠀⢸⡇⠀⠀\n⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⠀⠀⣤⠀⠉⣿⡇⠀⢠⡄⠈⣷⡇⠀⢸⡇⠀⢸⣿⠉⠉⠋⠀⢀⡗⠒⠛⠋⠉⠀⠀⠀⢀⣾⠇⠀⠀\n⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⠀⠀⠻⠀⠀⣽⡇⠀⢸⡇⠀⣻⣇⠀⠸⠇⠀⢸⣿⠶⠶⡟⠛⠉⠀⠀⠀⠀⠀⢀⣀⣴⡾⠋⠀⠀⠀\n⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⠦⠶⠴⠦⠒⠋⠷⠤⠞⠳⠤⠟⠈⠳⠤⠤⠶⠋⠙⠦⠤⠟⠻⠷⠶⠶⠾⠿⠛⠛⠋⠁⠀")
    def snap(self, p1, p2):
        if [math.floor(i) for i in p1] == [math.floor(i) for i in p2]:
            return 0
        else:
            return 1
        
    def write(self, entity, pos,cp=1):
        entity.pos = pos
        if cp:
            entity = copy.deepcopy(entity)
        try:
            key = tuple([math.floor(x/CELL_SIZE) for x in pos])
        except:
            return 
        if key in self.mmap:
            self.mmap[key].append(entity)
        else:
            self.mmap[key] = [entity]
        self.ref[entity.od] = key
    
    def run(self):
        keys_to_remove = []
        kwtw = []
        for key ,value in self.mmap.items():
            if len(value) <1:
                keys_to_remove.append(key)
            else:
                self.mmap[key] = [i.run() for i in self.mmap[key]]
                rmi = [self.mmap[key].pop(i) for i in range(len(self.mmap[key])-1, -1, -1) if self.snap(self.mmap[key][i].pos,list(key))]
                delthis = [self.mmap[key].pop(i) for i in range(len(self.mmap[key])-1, -1, -1) if self.mmap[key][i].delme]
                for i in rmi:
                    kwtw.append(i)
                    
                
        for i in keys_to_remove:
            del(self.ref[self.mmap[i].od])
            del(self.mmap[i])
            
        for i in kwtw:
            self.write(i,i.pos,cp=0)
        del(kwtw)
        del(keys_to_remove)                
            
